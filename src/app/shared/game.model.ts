export class Game {
  constructor(
    public gameName: string,
    public gameImg: string,
    public gamePlatform: string,
    public gameDescription: string
  ) {
  }
}
