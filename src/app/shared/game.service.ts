import { Game } from './game.model';
import { EventEmitter, Injectable, Output } from '@angular/core';
@Injectable()

export class GameService{
  games: Game[] = [
    {gameName:'World of tanks', gameImg: 'https://lh3.googleusercontent.com/jDi91lOJnAY0mlzDxaXgw5OENFmuoitRg0p8zW1VDgpW484W93zbx7PkFQRDaDVDjC5uY_TBSo1kUMiF6yrUan4',
    gamePlatform: 'PlayStation4', gameDescription: 'World of Tanks — это более 600 бронированных машин середины ХХ века. ' +
        'В ваших руках окажутся лучшие танки эпохи - от легендарных Т-34 и ИС, ковавших победу Красной армии, до безумных идей ' +
        'гениев инженерной мысли, так и не добравшихся до конвейерной ленты. Это несколько десятков уникальных боевых локаций, ' +
        'гарантирующих тактическое разнообразие геймплея. Это 160-миллионная армия поклонников по всему миру.'},
    {gameName: 'MainCraft', gameImg: 'https://s3.gaming-cdn.com/images/products/5657/orig/minecraft-windows-10-edition-windows-10-edition-pc-game-microsoft-store-cover.jpg',
      gamePlatform: 'Windows', gameDescription: 'компьютерная инди-игра в жанре песочницы, созданная шведским программистом Маркусом Перссоном и выпущенная его компанией Mojang AB. ' +
        'Перссон опубликовал начальную версию игры в 2009 году; в конце 2011 года была выпущена стабильная версия для ПК Microsoft Windows, распространявшаяся через официальный сайт'
    },
    {gameName: 'Grand Theft Auto V', gameImg: 'http://toplogos.ru/images/logo-grand-theft-auto-v.png',
    gamePlatform: 'PlayStation4', gameDescription: 'мультиплатформенная компьютерная игра в жанре action-adventure с открытым миром, разработанная компанией Rockstar North и изданная компанией Rockstar Games'}
  ];
  @Output() newGame = new EventEmitter<Game[]>();

  getGames(){
    return this.games.slice();
  }
  getGamesByPlatform(platform: string){

    for(let i = 0; i < this.games.length; i++){
      const gamesByPlatform: Game[] = [];
      const existingPlatform = gamesByPlatform.find(game => game.gamePlatform === platform);
        if (existingPlatform) {
          // gamesByPlatform.push(this.games)
        }
    }
   };

  getGame(index: number){
    return this.games[index]
  }
  addGame(game: Game){
    this.games.push(game);
    this.newGame.emit(this.games)
  }

}
