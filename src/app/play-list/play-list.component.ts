import { Component, OnDestroy, OnInit } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.css']
})
export class PlayListComponent implements OnInit{
  games!: Game[];

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.newGame.subscribe((games: Game[]) => {
      this.games = games;
      this.games.find(game =>{
        this.gameService.getGamesByPlatform(game.gamePlatform)
      });
    })
  }

}

