import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.model';
import { GameService } from '../../shared/game.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css']
})
export class GameCardComponent implements OnInit {
  game!: Game;

  constructor(
    private route: ActivatedRoute,
    private dishService: GameService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const gameId = parseInt(this.route.snapshot.params['id']);
      this.game = this.dishService.getGame(gameId);
    })
  }

}
