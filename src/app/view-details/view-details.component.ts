import { Component, OnInit } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})
export class ViewDetailsComponent implements OnInit{
  game!:Game;
  constructor(private gameService: GameService, private route: ActivatedRoute) { }

    ngOnInit(): void {
      const gameId = parseInt(this.route.snapshot.params['id']);
      this.game = this.gameService.getGame(gameId);
      console.log(this.game)
    }

}
