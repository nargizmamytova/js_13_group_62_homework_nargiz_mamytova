import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {
  game!: Game;
  @ViewChild('gameName') gameName!: ElementRef;
  @ViewChild('gameImg') gameImg!: ElementRef;
  @ViewChild('gamePlatform') gamePlatform!: ElementRef;
  @ViewChild('gameDescription') gameDescription!: ElementRef;
  constructor(public gameService: GameService) { }

  ngOnInit(): void {
  }

  createGame() {
    const name = this.gameName.nativeElement.value;
    const img = this.gameImg.nativeElement.value;
    const platform = this.gamePlatform.nativeElement.value;
    const description = this.gameDescription.nativeElement.value;
    this.game = new Game(name, img, platform,description);
    this.gameService.addGame(this.game);
  }

}
