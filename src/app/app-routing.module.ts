import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateGameComponent } from './create-game/create-game.component';
import { PlayListComponent } from './play-list/play-list.component';
import { ViewDetailsComponent } from './view-details/view-details.component';
import { GameCardComponent } from './play-list/game-card/game-card.component';

const routes: Routes = [
  {path: 'create-game', component: CreateGameComponent},
  {path: 'view-details', component: ViewDetailsComponent},
  {path: 'play-list', component: PlayListComponent, children:[
      {path: ':id', component: GameCardComponent,}
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
