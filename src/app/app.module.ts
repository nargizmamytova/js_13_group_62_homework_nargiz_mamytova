import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayListComponent } from './play-list/play-list.component';
import { ViewDetailsComponent } from './view-details/view-details.component';
import { CreateGameComponent } from './create-game/create-game.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { GameService } from './shared/game.service';
import { FormsModule } from '@angular/forms';
import { GameCardComponent } from './play-list/game-card/game-card.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayListComponent,
    ViewDetailsComponent,
    CreateGameComponent,
    ToolbarComponent,
    GameCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
